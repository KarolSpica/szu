import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

@WebServlet("/ConnectDbase")
public class ConnectDbase extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        Connection con = null;

        try{
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/testdb", "SA", "");

            if(con!=null){
                System.out.print("Ok");
            }else {
                System.out.print("wrong");
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
