package repository;

import java.util.List;

import domain.User;

public interface UsersRepository {
	User getUserById(int id);
	void addUser(User user);
	User getUserByName(String name);
	List<User> getAllUsers();
}
