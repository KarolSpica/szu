package repository;

import java.util.ArrayList;
import java.util.List;

import domain.User;

public class Repository implements UsersRepository {
	
	private static List<User> dataBase = new ArrayList<User>();
	
	@Override
	public User getUserById(int id) {
		for(User user: dataBase) {
			if(user.getId()==id)
				return user;
		}
		return null;
	}

	@Override
	public void addUser(User user) {
		dataBase.add(user);

	}
	@Override
	public User getUserByName(String name) {
		for(User user: dataBase) {
			if(user.getUserName().equalsIgnoreCase(name))
				return user;
		}
		return null;
	}

	@Override
	public List<User> getAllUsers() {
		return dataBase;
	}
	
}
