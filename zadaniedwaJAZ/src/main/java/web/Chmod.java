package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.UserType;
import repository.Repository;

@WebServlet("/Chmod")
public class Chmod extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	public Chmod() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("userList")!=null) {
			String username = request.getParameter("username");
			changeToPremium(session,username);
			Repository repo = (Repository)session.getAttribute("userList");
			response.getWriter().println("nazwa uzytkownika" + repo.getUserByName(username).getUserName()+ "uprawnienia: " + repo.getUserByName(username).getUserType());
			return;
		} else {
			response.getWriter().println("sesja jest pusta");
			return;
		}
	}
	
	protected void changeToPremium(HttpSession session, String userName) {
		Repository repo = (Repository)session.getAttribute("userList");
		repo.getUserByName(userName).setUserType(UserType.PREMIUM);
		session.setAttribute("userList", repo);
		return;
	}
}

