package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import domain.UserType;
import repository.Repository;
import repository.UsersRepository;

@WebServlet("/AddUser")
public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		User user = createUserFromRequest(request);
		UsersRepository repository = new Repository();
		
		HttpSession session = request.getSession();
		session.setAttribute("userList", repository);
		
		//dodanie do kontekstu aplikcaji
		this.getServletConfig().getServletContext().setAttribute("userList", repository);
		
		repository.addUser(user);
		response.sendRedirect("success.jsp");
	}
	
	private User createUserFromRequest(HttpServletRequest request) {
		User newUser = new User();
		newUser.setUserName(request.getParameter("username"));
		newUser.setPassword(request.getParameter("password"));
		newUser.setEmail(request.getParameter("email"));
		newUser.setUserType(UserType.REGULAR);
		return newUser;
	}

}
