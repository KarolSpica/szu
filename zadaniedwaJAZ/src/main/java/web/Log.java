package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repository.Repository;

@WebServlet("/Log")
public class Log extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("userList")!=null) {
			String userName = request.getParameter("username");
			String password = request.getParameter("password"); 
			if(doUserExist(session, password, userName)) {
				response.sendRedirect("yourpage.jsp");
			}
		 else {
			response.getWriter().println("Bledne dane logowania");
		}
		}else {
			response.getWriter().println("Sesja nie istnieje");
		}
	}
	private boolean doUserExist(HttpSession session, String password, String userName) {
		Repository repo  =(Repository)session.getAttribute("userList");
		for(User user: repo.getAllUsers()) {
			if(user.getUserName().equalsIgnoreCase(userName) && user.getPassword().equalsIgnoreCase(password)) {
				return true;
			}
		}
		return false;
	}

}
