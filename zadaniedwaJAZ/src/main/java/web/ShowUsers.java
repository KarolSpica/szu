package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repository.Repository;

@WebServlet("/ShowUsers")
public class ShowUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ShowUsers() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("userList")!=null) {
			Repository repo = (Repository)session.getAttribute("userList");
			response.getWriter().print(createUserTable(repo));
			return;
		} else {
			response.getWriter().println("sesja jest pusta");
			return;
		}
	}
	public String createUserTable(Repository repo) {
		String table = "<table style="+'"'+"border: 1px solid black; margin-top:10px; margin-left:10px;"+'"'+">";
		for(User user: repo.getAllUsers()) {
			table += "<tr><th>"+user.getUserName()+"</th><th>"+user.getUserType()+"</th></tr>";
		}
		table += "</table>";
		
		return table;
	}

}
