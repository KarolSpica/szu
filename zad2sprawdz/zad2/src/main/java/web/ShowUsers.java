package web;

import domain.User;
import domain.UserType;
import repository.Repository;
import repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/ShowUsers")
public class ShowUsers extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("currentUser", UserType.ADMIN);

        Repository repo = new UserRepository();

        request.setAttribute("message", createTableOfUsers(repo));
        request.getRequestDispatcher("/chmod.jsp").forward(request, response);
    }

    public String createTableOfUsers(Repository repo){
        String table = "";
        for(User user: repo.getAllUsers()) {
            table +=("<tr><th>"+user.getUsername()+"</th><th>"+user.getUsertype()+"</th></tr><br/>");
        }
        return  table;
    }
}
