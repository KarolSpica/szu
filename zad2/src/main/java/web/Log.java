package web;

import domain.User;
import domain.UserType;
import repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/Log")
public class Log extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("currentUser", UserType.ANONIM);

        UserRepository repo = new UserRepository();
        changeSessionAtribute(session, repo, request);
        if(session.getAttribute("currentUser")==UserType.REGULAR) {
            response.sendRedirect("usersite.jsp");
        } else if(session.getAttribute("currentUser")==UserType.PREMIUM) {
            response.sendRedirect("premium.jsp");
        } else if(session.getAttribute("currentUser")==UserType.ADMIN) {
            response.sendRedirect("chmod.jsp");
        }
    }

    private void changeSessionAtribute(HttpSession session, UserRepository repo, HttpServletRequest request){
        session.setAttribute("currentUser", repo.getUserByName(request.getParameter("username")).getUsertype());
    }
}
