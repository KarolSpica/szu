package web.filters;

import domain.User;
import repository.UserRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/Log")
public class LogFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();

        UserRepository repo = new UserRepository();
        if(checkDoUserIsInDatabase(request, repo)){
                filterChain.doFilter(request,response);
                return;
        } else {
           response.getWriter().print("Zaloz konto");
        }
    }

    @Override
    public void destroy() {

    }
    private boolean checkDoUserIsInDatabase(ServletRequest request, UserRepository repo){
        String userName = request.getParameter("username");
        String userPassword = request.getParameter("password");
        User user = repo.getUserByName(userName);

        if(userName.equals(user.getUsername()) && userPassword.equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }
}
