package web.filters;

import domain.UserType;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/premium.jsp", "/chmod.jsp"})
public class SiteFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();

        if(session.getAttribute("currentUser")== UserType.ANONIM || session.getAttribute("currentUser")==UserType.REGULAR || session.getAttribute("currentUser")==null) {
            response.getWriter().print("Nie masz dostepu do tej strony");
        }
        else {
            filterChain.doFilter(request,response);
            return;
        }
    }

    @Override
    public void destroy() {

    }
}
